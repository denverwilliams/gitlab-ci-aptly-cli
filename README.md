# Aptly CLI container for Gitlab CI

This project fetches artifacts from any Gitlab project (via API) and upload Debian packages to any aptly repository using [aptly\_cli](https://github.com/sepulworld/aptly_cli). It builds on triggers, preferably when another project has built some packages.

The process utilizes a lot of variables, which contains private data (like tokens, usernames, passwords,...). You might pass them via [trigger variables](http://docs.gitlab.com/ce/ci/triggers/README.html), but then they will appear on the build job. It is advised to fork this repo and add your private secure variables. Also you need to enable at least one trigger to get a token.

## Private variables

Set these in `Project > Variables > Add Variable`.

![](secret-variables.png)

### mandatory

* ```BASE_URL```: defines the base url of the Gitlab instance from where to fetch artifacts (e.g. ```https://gitlab.com```)
* ```PRIVATE_TOKEN```: any private token, which has sufficient access to the Gitlab project, you want to fetch from
* ```REPO_API_HOST```: defines the aptly API url (e.g. ```my-repo.my-domain.com```)
* ```REPO_API_PORT```: defines the aptly API port (e.g. ```8080```)
 
### optional

* ```REPO_API_USER```: If your aptly API is [protected](https://blog.morph027.de/post/protect-aptly-api-with-basic-authentication/), set these credentials
* ```REPO_API_PASSWORD```: see above
* ```GPG_KEY```: defines the GPG key id to use for signing (e.g. ```4F80269B```)
* ```GPG_PASSPHRASE```: if not using passwordless GPG key, this defines the passphrase to unlock the key

## public variables

Can safely be passed by triggers (e.g ```curl ... -F "variables[REPO_NAME]=my-repo" ...```)

* ```REPO_API_DEBUG```: defines if [aptly-cli](https://github.com/sepulworld/aptly_cli) prints debug information (```true|false```)
* ```REPO_DIST```: defines the distribution for the published aptly repo (e.g. ```jessie```, ```xenial```)

Also, all other mandatory variables (e.g. ```PROJECT```, ```STAGE```) from [gitlab-ci-helpers](https://gitlab.com/morph027/gitlab-ci-helpers#get-last-successful-build-artifactsh) must be set to successfully fetch the artifacts. See the helpers project for mor info (e.g. how to get the project id,..).

Optional variables (e.g. ```REF```, ```COMMIT```) can be set too.

## Example

This one is fetching [SOGo packages](https://gitlab.com/packaging/sogo) and push them to my [personal repo](https://repo.morph027.de/).

```
curl -X POST \
-F token=xxxxxxxxxxxxxxxxxxxxxx \
-F ref=master \
-F "variables[REPO_NAME]=sogo" \
-F "variables[REPO_DIST]=jessie" \
-F "variables[PROJECT]=1603666" \
-F "variables[STAGE]=test" \
-F "variables[REF]=master" \
https://gitlab.com/api/v3/projects/1604067/trigger/builds
```
