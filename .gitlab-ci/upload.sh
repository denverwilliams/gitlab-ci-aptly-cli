#!/bin/bash

export VAR=( REPO_API_HOST REPO_API_PORT REPO_API_DEBUG REPO_NAME REPO_DIST )
. <(curl -sL https://gitlab.com/morph027/gitlab-ci-helpers/raw/master/check-vars.sh)
. <(curl -sL https://gitlab.com/morph027/gitlab-ci-helpers/raw/master/get-last-successful-build-artifact.sh)
download_latest

unzip -q -l artifacts.zip 2>&1 | grep '\.deb' > /dev/null

if [ $? -eq 0 ]; then
  unzip artifacts.zip
  . ${CI_PROJECT_DIR}/.gitlab-ci/lib-aptly.sh
  for PACKAGE_FILE in $(find . -type f -name '*.deb')
  do
    upload
    add
  done
  publish
fi
