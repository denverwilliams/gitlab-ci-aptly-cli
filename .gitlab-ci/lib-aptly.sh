#!/bin/bash

# check, if aptly-cli is available

which aptly-cli > /dev/null || ( echo "Sorry, aptly-cli is missing"; exit 1)

# add aptly config file

cat > /etc/aptly-cli.conf << EOF
---
:server: $REPO_API_HOST
:port: $REPO_API_PORT
:debug: $REPO_API_DEBUG
EOF

# if set, add username + password to aptly conf

[ ! -z $REPO_API_USER ] && echo ":username: $REPO_API_USER" >> /etc/aptly-cli.conf
[ ! -z $REPO_API_PASSWORD ] && echo ":password: $REPO_API_PASSWORD" >> /etc/aptly-cli.conf

# define functions

upload() {
  echo "uploading file $1 ..."
  aptly-cli file_upload --directory /ci-upload-${REPO_NAME} --upload ${PACKAGE_FILE}
}

add() {
  echo "adding uploaded file to repo ..."
  aptly-cli repo_upload --name ${REPO_NAME} --dir /ci-upload-${REPO_NAME} --file ${PACKAGE_FILE##*/} --forcereplace
}

publish() {
  echo "publishing repo ..."
  local APTLY_CLI_OPTS="--forceoverwrite"
  [ ! -z $GPG_KEY ] && APTLY_CLI_OPTS="$APTLY_CLI_OPTS --gpg_key $GPG_KEY"
  [ ! -z $GPG_PASSPHRASE ] && APTLY_CLI_OPTS="$APTLY_CLI_OPTS --gpg_passphrase $GPG_PASSPHRASE --gpg_batch"
  aptly-cli publish_update --prefix ${REPO_NAME} --distribution ${REPO_DIST} "$APTLY_CLI_OPTS"
}
